import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tekab_dev_test/utils/colors.dart';

class HeaderCart extends StatelessWidget {
  const HeaderCart({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Get.height * 0.095,
      width: Get.width * 0.9,
      decoration:  BoxDecoration(
          color: Colors.white,
          borderRadius: const BorderRadius.only(
            topRight: Radius.circular(20),
            bottomLeft: Radius.circular(20),
            bottomRight: Radius.circular(20),
          ),boxShadow: [
        BoxShadow(
          color: Colors.grey.withOpacity(0.5),
          spreadRadius: 3,
          blurRadius: 5,
          offset: const Offset(0, 2), // changes position of shadow
        ),
      ],),
      child: Row(
        children: [
          Image.asset(
            'assets/images/avatar.png',
            scale: 2,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                const Text(
                  "Urbain amand",
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 16,
                      fontWeight: FontWeight.w700),
                ),
                const Text(
                  "urbain.amand@gmail.fr",
                  style: TextStyle(
                      fontFamily: 'Roboto',
                      fontSize: 12,
                      fontWeight: FontWeight.w400),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "Solde de point",
                      style: TextStyle(
                          fontFamily: 'Roboto',
                          fontSize: 12,
                          fontWeight: FontWeight.w400),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Container(
                      height: 25,
                      width: 50,
                      decoration: BoxDecoration(
                          color: AppColors.MAIN_COLOR,
                          borderRadius: BorderRadius.circular(15)),
                      child: const Center(
                        child: Text(
                          "523",
                          style: TextStyle(color: Colors.white,fontFamily: 'Roboto',
                              fontSize: 14,
                              fontWeight: FontWeight.w700),
                        ),
                      ),
                    ),


                  ],
                )
              ],
            ),
          ),
          SizedBox(width: Get.width*0.17,),
          Align(
            alignment: FractionalOffset.centerRight,
            child: Image.asset(
              'assets/images/ribon.png',
              scale: 2,
            ),
          ),
        ],
      ),
    );
  }
}
