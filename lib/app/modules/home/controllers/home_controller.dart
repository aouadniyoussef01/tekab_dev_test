import 'package:get/get.dart';

class HomeController extends GetxController {

  List<String> listTileItems = [
    "Sondages",
    "Cartes cadeaux",
    "Jeux concours",
    "Parrainages"
  ];
  List<String> icons=[
    'assets/icons/question.png',
    'assets/icons/gift.png',
    'assets/icons/circles.png',
    'assets/icons/heart.png',
  ];
  List<String> texts=[
    'Historique',
    'Nous contacter',
    'FAQ',
    'Se deconnecter',
  ];
  List<String> routs=['/sondage'];
  @override
  void onInit() {
    super.onInit();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {
    super.onClose();
  }

}
