import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tekab_dev_test/components/header_cart.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  const HomeView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: Get.width,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage('assets/images/screen_container.png'),
                fit: BoxFit.fitWidth
          )
        ),
        child: Padding(
          padding: const EdgeInsets.only(top: 50.0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              const HeaderCart(),
              Flexible(
                fit: FlexFit.loose,
                child: CustomScrollView(
                  slivers: [
                    SliverList(
                        delegate: SliverChildBuilderDelegate(
                              (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.only(top: 10.0,left: 10),
                              child: ListTile(
                                title: Text(
                                  controller.listTileItems[index],
                                  style: const TextStyle(
                                      fontFamily: 'Roboto',
                                      fontWeight: FontWeight.w500,
                                      fontSize: 16,color: Colors.white),
                                ),
                                leading: Image.asset(
                                  controller.icons[index],
                                  scale: 2,
                                ),
                                onTap: (){
                                  Get.toNamed(controller.routs[index]);
                                },
                              ),
                            );
                          },
                          childCount: controller.listTileItems.length,
                        )),
                    SliverList(
                      delegate: SliverChildBuilderDelegate(
                              (BuildContext context, int index) {
                            return InkWell(
                              child: Padding(
                                padding: const EdgeInsets.only(left: 30.0,top: 20),
                                child: Text(controller.texts[index],style: const TextStyle(color: Colors.white),),
                              ),
                            );
                          }, childCount: controller.texts.length),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(bottom: 60.0,right: 10),
                child: Align(
                  alignment: FractionalOffset.bottomRight,
                    child: Image.asset('assets/images/person.png',scale: 2,)),
              )

            ],
          ),
        ),
      ),
    );
  }
}
