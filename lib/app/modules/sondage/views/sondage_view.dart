import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:tekab_dev_test/utils/colors.dart';

import '../controllers/sondage_controller.dart';

class SondageView extends GetView<SondageController> {
  const SondageView({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: Stack(
        alignment: FractionalOffset.topCenter,
        children: [
          Image.asset(
            'assets/images/top_container.png',
          ),
          Padding(
            padding: const EdgeInsets.only(top: 50.0, left: 20),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Image.asset(
                      'assets/icons/nav_bar.png',
                      scale: 1.5,
                    ),
                    Stack(
                      clipBehavior: Clip.none,
                      alignment: FractionalOffset.topLeft,
                      children: [
                        Container(
                          height: 40,
                          width: 80,
                          decoration:  BoxDecoration(
                              color: const Color.fromRGBO(69, 99, 199, 1),
                              borderRadius: const BorderRadius.only(
                                  bottomLeft: Radius.circular(15)),
                            boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.5),
                              spreadRadius: 3,
                              blurRadius: 5,
                              offset: const Offset(0, 2), // changes position of shadow
                            ),
                          ],
                          ),
                          child: const Center(
                              child: Text(
                            "120",
                            style: TextStyle(color: Colors.white),
                          )),
                        ),
                        Positioned(
                            bottom: 20,
                            right: 60,
                            child: Image.asset(
                              'assets/icons/second_gift.png',
                              scale: 2,
                            )),
                      ],
                    )
                  ],
                ),
              ],
            ),
          ),
          const Padding(
            padding: EdgeInsets.only(top: 110.0),
            child: Text(
              'Sondage d\'aujourd\'hui',
              style: TextStyle(
                  color: Colors.white,
                  fontFamily: 'Roboto',
                  fontSize: 16,
                  fontWeight: FontWeight.w700),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 160.0),
            child: Column(
              children: [
                Image.asset(
                  'assets/images/food.png',
                  scale: 2.5,
                ),
                const SizedBox(
                  height: 20,
                ),
                Text(
                  'Votre chiffre clé'.toUpperCase(),
                  style: const TextStyle(
                      color: Color.fromRGBO(69, 99, 199, 1),
                      fontFamily: 'Roboto',
                      fontSize: 20,
                      fontWeight: FontWeight.w600),
                ),
              ],
            ),
          ),
          Align(
            alignment: FractionalOffset.bottomLeft,
            child: Stack(
                clipBehavior: Clip.none,
                alignment: FractionalOffset.bottomLeft,
                children: [
                  Image.asset('assets/images/decoration.png',
                      fit: BoxFit.contain),
                  Padding(
                    padding: const EdgeInsets.only(left: 20.0),
                    child: SizedBox(
                      height: Get.height * 0.45,
                      width: Get.width,
                      child: GridView.builder(
                          padding: const EdgeInsets.only(bottom: 10),
                          gridDelegate:
                              const SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisSpacing: 0,
                                  mainAxisSpacing: 1,
                                  crossAxisCount: 2,
                                  childAspectRatio: 1),
                          itemCount: 4,
                          itemBuilder: (BuildContext context, int index) {
                            return Stack(children: [
                              Image.asset(
                                'assets/images/container.png',
                                scale: 1.7,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(top: 20),
                                child: Column(
                                  children: const [
                                    Center(
                                      child: Text(
                                        '500',
                                        style: TextStyle(
                                            color: AppColors.MAIN_COLOR,
                                            fontFamily: 'Roboto',
                                            fontSize: 24,
                                            fontWeight: FontWeight.w700),
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsets.only(right: 10.0),
                                      child: Align(
                                        alignment: FractionalOffset.center,
                                        child: SizedBox(
                                          height: 100,
                                          width: 150,
                                          child: Text(
                                            textAlign: TextAlign.center,
                                            'Lorem Ipsum est simplement du faux texte',
                                            maxLines: 4,
                                            softWrap: false,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                color: AppColors.MAIN_COLOR,
                                                fontFamily: 'Roboto',
                                                fontSize: 14,
                                                fontWeight: FontWeight.w400),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ]);
                          }),
                    ),
                  ),
                ]),
          )
        ],
      ),
    );
  }
}
